## Purpose
The purpose of this method is to estimate the number Pi using a probabilistic method : Monte Carlo method.

---

## Description of this work
Before coding, we approach this problem with a geometrical reasoning and we describe the configuration we are in.

### Defining the region we work in
We place ourselves in an orthonormed two dimensional system. We draw a 2*2 g.u. (graphical unit) square, centered on the origin and we draw its inscribed circle (the circle is then centered too, duh).
Once done, we have a square and a one g.u. radius circle that are divided in four quadrants. The first trick of this method is to place ourselves in one of the four quadrants to increase the algorithm's accuracy by four.
Because I found it more intuitive, I decided to work in the upper-right quadrant region.

The next step is to pick random points inside the region I chose to work in and to determine how many points are strictly in the quart of a circle. 

And here comes the second trick ! The ratio of ![my equation](https://latex.codecogs.com/gif.download?nb_%7Bin-circle%7D) (the number of point that are inside the circle) divided by ![my equation](https://latex.codecogs.com/gif.download?nb_%7Bpoints%7D) (the number of points randomly picked), approximates pi on four !

### Why and how Monte Carlo method is used to solve our problem
Monte Carlo's methods are caracterized as probabilistic approaches because they work with random values. In our case, using this method seems relevant.

1. It is easy to code this method. 
2. It is kind of intuitive. The ratio ![my equation](https://latex.codecogs.com/gif.download?N%3D%5Cfrac%7Bnb_%7Bin-circle%7D%7D%7Bnb_%7Bpoints%7D%7D) can be interpreted as "how much the surface of the circle covers the surface of the region" if the value of ![my equation](https://latex.codecogs.com/gif.download?nb_%7Bpoints%7D) is large enough. Therefore it can approximate the ratio ![my equation](https://latex.codecogs.com/gif.download?R%20%3D%20%5Cfrac%7BS_%7Bcircle%7D%7D%7BS_%7Bregion%7D%7D) (where ![my equation](https://latex.codecogs.com/gif.download?S_%7Bcircle%7D) is the surface value of the quart of a circle & ![my equation](https://latex.codecogs.com/gif.download?S_%7Bregion%7D) is the quadrant's surface value).
3. Thanks to this last statement, we can obtain a neat estimation pi. The ratio ![my equation](https://latex.codecogs.com/gif.download?R) can be easily determined numerically : ![my equation](https://latex.codecogs.com/gif.download?N%20%5Capprox%20R%20%3D%20%5Cfrac%7BS_%7Bcircle%7D%7D%7BS_%7Bregion%7D%7D%20%3D%20%5Cfrac%7B%5Cfrac%7B%5Cpi%20%5Ctimes%20%7Br_c%7D%5E2%7D%7B4%7D%7D%7B%7Br_c%7D%5E2%7D%20%3D%20%5Cfrac%7B%5Cpi%7D%7B4%7D) (where ![my equation](https://latex.codecogs.com/gif.download?r_c) the radius of the circle).

For further explanations (if mine are not clear enough), go see : https://en.wikipedia.org/wiki/Monte_Carlo_method

---

## Limitations of this method
Although my explanations may not appear clear enough, this algorithm is pretty simple for estimating pi (conceptualy speaking at least). However, it has several flaws: 

1. It requires lots of points to have correct and consistent approximations. This takes lots of time to render, which is a shame. 
2. Because of that, the algorithm's convergence is really slow ! Modern algorithms for estimating pi can approximate the latter first hundred digits' in twenty steps (see https://en.wikipedia.org/wiki/Approximations_of_π#Development_of_efficient_formulae).

---

## Personal initiative
As you must have guessed, I AM NOT the inventor of this method and I am sure some codes exhibiting Monte Carlo method for approximating pi already exist on the Internet.
However, I MADE THIS PROGRAMM MYSELF (I DID NOT copy&paste any work material for approximating pi with Monte Carlo methods). I wanted to train myself in C, to have a better grasp on the random library integrated in `<stdlib.h>` using `<time.h>`. 
Of course, C is not the most appropriate for this purpose as I am pretty familiar with Python and its matplotlib.pyplot library. With Python I would have got some really cool results in no time I think, with a graphical illustration of the randomized points. But again, I wanted this training to be in C.

---

## Compiling the code

* If you work with an IDE: pasting the code to compile it in C is easy.
* Otherwise : as I am using Visual Studio Code (VSC), you will find a Makefile that compiles the main.c file with a gcc command. VSC is not necessary to compile the code. All you need to do is opening the program's directory in the Terminal with `$ cd` commands (in VSC, doing **Terminal>New Terminal** and `cd script/` in Terminal accesses the right directory). To test the code, type `make` (errors should not occur) then `./results`. The program will run in the console.