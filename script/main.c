# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <time.h>

float Randomize(int upper, int lower) {
    float random = ((float) rand()/(RAND_MAX))*(upper - lower);
    return(random);
}

int main(int argc, char const *argv[])
{
    srand(time(NULL)); // Initializing srand() to initialize a sorted set of ints that will be used for computation
    float x ; float y ; float radius ;
    int nb_points ; int nb_in_circle = 1 ; int nb_method = 0 ;
    // Initialization in console
    printf("Initializing algorithm...\n");

    printf("Number of points?\n");
    scanf("%d",&nb_points);
    
    // Monte Carlo method
    for (int i=1 ; i<nb_points ; i++) {
        x = Randomize(1,0);
        y = Randomize(1,0);

        radius = powf(x,2) + powf(y,2);
        radius = sqrtf(radius);
        printf("x: %f\t y: %f\t R: %f\t Iteration : %d/%d\n",x,y,radius,i,nb_points);

        if (radius <= 1.0) {
            nb_in_circle++;
        }
    }
    
    float pi = ((float) nb_in_circle/nb_points)*4; 
    printf("\n\nNoP : %d\t NiC : %d\t Estimation of pi: %f",nb_points,nb_in_circle,pi);
    return 0;
}
